App.Post = DS.Model.extend({
  title: DS.attr('string'),
  slug: DS.attr('string'),
  author: DS.attr('string'),
  excerpt: DS.attr('string')
});

App.Post.FIXTURES = [
  {
    id: 1,
    slug: 'post-one',
    title: 'my first post',
    author: 'writer1',
    excerpt: 'some text here'
  }, {
    id: 2,
    slug: 'post-two',
    title: 'my 2nd post',
    author: 'writer2',
    excerpt: 'some awesome text here'
  }, {
    id: 3,
    slug: 'post-three',
    title: 'my 3rd post',
    author: 'writer1',
    excerpt: '3333'
  }, {
    id: 4,
    slug: 'post-four',
    title: 'my 4th post',
    author: 'writer2',
    excerpt: '4444'
  }
];
