App.Router.map(function() {
  return this.resource('posts');
});

App.IndexRoute = Ember.Route.extend({
  redirect: function() {
    return this.transitionTo('posts');
  }
});

App.PostsRoute = Ember.Route.extend({
  model: function() {
    return this.store.find('post');
  }
});
