# App.Router.reopen {
#   location: 'history'
# }

App.Router.map ->
  @.resource 'posts'

App.IndexRoute = Ember.Route.extend
  redirect: ->
    @.transitionTo 'posts'

App.PostsRoute = Ember.Route.extend
  model: ->
    @.store.find 'post'