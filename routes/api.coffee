fs = require 'fs'
express = require 'express'
solr = require 'solr-client'
TwitterAPI = require 'node-twitter-api'

accessToken = '59860290-UxFSKXC99J3EWMRB21zI9ahAQg4znIk8OO02ubwFx'
accessTokenSecret = 'tTsjsea1RXX70tJghHdZ3JuAefp5lhFdrrOcJMWBE34jE'

router = express.Router()

###
POST call to /api/tweets will:
1) search Twitter for tweets
2) format JSON for Solr
3) log data to log.txt file
4) add docs to Solr if tweets found
eg: curl --data '{"q":"vancouver"}' -H 'Content-type:application/json' http://localhost:3000/api/tweets
###
router.post '/tweets', (req, res) ->

  # ping solr client..
  # client.ping (err,obj) ->
  #    if (err)
  #     console.log err
  #    else
  #     console.log obj
  #     res.send obj

  # grab tweets..
  twitter = new TwitterAPI
    consumerKey: 'eR1SY7e9vbngOw08ZZysXnWpM'
    consumerSecret: '1yV1LnWTmG3C0SH8xRjasqQZeDxfhlhcALXxc2aXRulI4a4bXe'
    callback: 'http://localhost:3000'

  # twitter.verifyCredentials accessToken, accessTokenSecret, (error, data, response) ->
  #   if (error)
  #     console.log 'error >>', error
  #   else
  #     #accessToken and accessTokenSecret can now be used to make api-calls (not yet implemented)
  #     #data contains the user-data described in the official Twitter-API-docs
  #     #you could e.g. display his screen_name
  #     console.log data

  tweets = []
  twitter.search { q: req.body.q }, accessToken, accessTokenSecret, (error, data, response) ->
    if (error)
      console.log error
    else
      # console.log data
      data.statuses.forEach (item) ->
        if item.lang is 'en'
          date = new Date item.created_at
          tweet = {
            id: item.id
            timestamp_tdt: date.toISOString()
            text: item.text
            user_id_s: item.user.id
            user_name_s: item.user.name
            screen_name_s: item.user.screen_name
            user_profile_img: item.user.profile_image_url
            lang: item.lang
          }
          tweets.push tweet

      info = {
        success: true
        tweets: tweets.length
        data: tweets
      }

      fs.appendFile 'log.txt', JSON.stringify(info) + '\n', (err) ->
        if (err)
          console.log 'error appending to log.txt..'

      if info.tweets > 0
        # add tweets to Solr
        client = solr.createClient('127.0.0.1', '8983', 'tweeters')
        client.add info.data, (err, obj) ->
          if (err)
            console.log err
          else
            console.log 'Solr response:', obj
            client.commit (err,res) ->
              if (err) then console.log err
              if (res) then console.log res

      res.send info

module.exports = router
