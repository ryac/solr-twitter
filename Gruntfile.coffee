module.exports = (grunt) ->

  # Tasks
  grunt.initConfig {

    # ----------------------
    # Clean
    # ----------------------
    # clean:
    #   src: ['!public/js/templates.js', 'public/js']
    #   templates: ['public/js/templates.js']

    # ----------------------
    # Coffee
    # ----------------------
    coffee:
      dev:
        options:
          bare: true
        expand: true
        cwd: 'public/coffee/'
        src: ['**.coffee', '**/*.coffee']
        dest: 'public/js/'
        ext: '.js'

    # ----------------------
    # Watch
    # ----------------------
    watch:
      scripts:
        files: 'public/coffee/**/*.coffee'
        tasks: ['coffee']
      emberTemplates:
        files: 'public/templates/**/*.hbs',
        tasks: ['emberTemplates']

    # --------------------------------------
    # Compile Handlebars templates for Ember
    # --------------------------------------
    emberTemplates:
      compile:
        options:
          templateCompilerPath: 'public/bower_components/ember/ember-template-compiler.js'
          handlebarsPath: 'public/bower_components/handlebars/handlebars.min.js'
          templateBasePath: 'public/templates'
          precompile: false
        files:
          'public/js/templates.js': 'public/templates/**/*.hbs'
  }

  # Plugins
  require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks)

  # Commands
  grunt.registerTask 'default', ['']

  return grunt